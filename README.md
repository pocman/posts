# Test Back-end - iAdvize - Posts

[![build status](https://gitlab.com/pocman/posts/badges/master/build.svg)](https://gitlab.com/pocman/posts/commits/master)
[![coverage report](https://gitlab.com/pocman/posts/badges/master/coverage.svg)](https://gitlab.com/pocman/posts/commits/master)

## Notes
I had to use UUID as identifiers for the posts. Sequential Ids are not suitable in a fully asynchronous world.

## Todos
* Switch to VDM api or VDM Rss feed
* Improve Cassandra Querying (Add type system)

## Execution
### Run Tests

Open a terminal and execute : `sbt test`

### Run in Dev

Open a terminal and execute : `sbt runAll` 

Wait for the services to start Cassandra, Kafka, Gateway, ServiceLocator and to register PostService.
Scrapping will be executed when the service is started. To relaunch, run ./scrap.sh
```
...
[info] Cassandra server running at 127.0.0.1:4000
[info] Service locator is running at http://localhost:8000
[info] Service gateway is running at http://localhost:9000
[info] Service post-impl listening for HTTP on 0:0:0:0:0:0:0:0:53037
[info] (Service started, press enter to stop and go back to the console...)
[info] HTTP/1.1 200 OK
```

PostService is exposed at `http://localhost:9000/`

### Run in Prod

For deployment on Kubernetes see : [Github](https://github.com/typesafehub/service-locator-dns)

For static standalone deployment (Play like) see : [Documentation](http://www.lagomframework.com/documentation/1.2.x/java/Overview.html#deploying-to-other-platforms)