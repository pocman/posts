All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

More infos about this file : http://keepachangelog.com/

## [Unreleased] - no_due_date

## v1.0.0 - 11-12-2016
* Async scrapping, search and get PostService API
* VDM Scapper using Jsoup
