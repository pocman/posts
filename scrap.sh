#!/bin/sh


# Try to scrap until Service returns 200 OK

response=""
while [ "$response" == "" ]
do
response=$(wget -S -t 0 --retry-connrefused -O /dev/null "http://localhost:9000/api/scrap?limit=200" 2>&1 | egrep -i "200 OK")
done
echo $response
