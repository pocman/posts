package com.colisweb

import com.lightbend.lagom.javadsl.testkit.ServiceTest._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.mockito.MockitoSugar
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, FlatSpec, Matchers}
import play.inject.guice.GuiceApplicationBuilder

trait ServiceSpec extends FlatSpec
  with Matchers
  with MockitoSugar
  with ScalaFutures
  with BeforeAndAfterAll
  with BeforeAndAfterEach {

  def configureBuilder(b: GuiceApplicationBuilder) = b

  implicit val defaultPatience = PatienceConfig(
    timeout = Span(10, Seconds),
    interval = Span(500, Millis)
  )

  var server: TestServer = _

  override def beforeAll(): Unit = {
    server = startServer(defaultSetup.withCassandra(true).withCluster(true).configureBuilder(configureBuilder))
  }

  override def afterAll(): Unit = {
    server.stop()
  }

}
