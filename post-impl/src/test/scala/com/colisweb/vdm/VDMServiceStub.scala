package com.colisweb.vdm

import akka.NotUsed
import akka.stream.scaladsl.Source
import com.colisweb.post.api.PostService.PostRequest
import com.colisweb.post.impl.Generators

class VDMServiceStub extends VDMService {
  /**
    * @return Unlimited source of Post from Generator
    */
  override def scrapPages: Source[PostRequest, NotUsed] =
    Source.fromIterator(() => Stream.from(0).toIterator).mapConcat(_ => Generators.genPostRequest.sample.toList)
}
