package com.colisweb.vdm

import akka.stream.scaladsl.Sink
import com.colisweb.ServiceSpec
import com.colisweb.post.api.PostService.PostRequest
import com.google.inject.AbstractModule
import org.scalacheck.Gen
import play.inject.guice.GuiceApplicationBuilder

import scala.collection.immutable.Seq
import scala.concurrent.Await
import scala.concurrent.duration._

class VDMScrapperSpec extends ServiceSpec {

  def vdmService: VDMService = server.injector.instanceOf(classOf[VDMService])

  class PostServiceImplModule extends AbstractModule {
    def configure() = {
      bind(classOf[VDMService]).to(classOf[VDMScrapper])
    }
  }

  override def configureBuilder(b: GuiceApplicationBuilder) = b.overrides(new PostServiceImplModule)

  it should "scrap posts from vdm website" in {
    val limit = Gen.chooseNum[Long](1, 10).sample.get
    val posts: Seq[PostRequest] = Await.result(
      vdmService.scrapPages.take(limit).runWith(Sink.seq)(server.materializer),
      30 seconds
    )

    posts.size shouldBe limit
    posts.distinct.size shouldBe limit
  }

}
