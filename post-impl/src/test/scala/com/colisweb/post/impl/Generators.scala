package com.colisweb.post.impl

import com.colisweb.post.api.PostService.PostRequest
import com.colisweb.post.impl.entities.Post
import com.fortysevendeg.scalacheck.datetime.jdk8.GenJdk8._
import org.scalacheck.Gen

object Generators {

  import com.fortysevendeg.scalacheck.datetime.jdk8.granularity.minutes

  def genPostRequest: Gen[PostRequest] =
    for {
      content <- Gen.alphaStr
      author <- Gen.alphaStr
      date <- genZonedDateTime.map(_.toLocalDateTime).map(_.withYear(2017))
    } yield PostRequest(content = content, date = date, author = author)

  def genPost: Gen[Post] =
    for {
      id <- Gen.uuid
      content <- Gen.alphaStr
      author <- Gen.alphaStr
      date <- genZonedDateTime.map(_.toLocalDateTime)
    } yield Post(id = id, content = content, date = date, author = author)

}
