package com.colisweb.post.impl

import java.util.UUID

import akka.Done
import com.colisweb.post.api.exceptions.{PostAlreadyCreatedException, PostNotFoundException}
import com.colisweb.post.impl.entities._
import com.lightbend.lagom.javadsl.persistence.PersistentEntity.InvalidCommandException
import com.lightbend.lagom.javadsl.testkit.PersistentEntityTestDriver
import org.scalatest.prop.PropertyChecks

class PostEntitySpec extends EntitySpec with PropertyChecks {

  it should "create post" in {
    forAll(Generators.genPost) { (post: Post) =>
      val (entityId, _, outcomeInformation) = createPost(post)

      outcomeInformation.events.head shouldBe PostCreated(UUID.fromString(entityId), post)
      outcomeInformation.state shouldBe PostState(mayPost = Some(post))
      outcomeInformation.replies.head shouldBe Done
    }
  }

  it should "throw invalid command when trying to create on already created entity" in {
    forAll(Generators.genPost) { (post: Post) =>
      val (_, driver, _) = createPost(post)
      getOutcomeInformation(driver.run(CreatePost(post))).replies.head shouldBe PostAlreadyCreatedException
    }
  }


  it should "get post" in {
    forAll(Generators.genPost) { (post: Post) =>
      val (_, driver, _) = createPost(post)
      val outcomeInformation = getOutcomeInformation(driver.run(GetPost()))

      outcomeInformation.events shouldBe Nil
      outcomeInformation.state shouldBe PostState(mayPost = Some(post))
      outcomeInformation.replies.head shouldBe post
    }
  }

  it should "throw invalid command when trying to get empty entity" in {
    forAll(Generators.genPost) { (post: Post) =>
      val entityId = UUID.randomUUID().toString
      val driver = new PersistentEntityTestDriver[PostCommand, PostEvent, PostState](system, new PostEntity(), entityId)
      getOutcomeInformation(driver.run(GetPost())).replies.head shouldBe PostNotFoundException
    }
  }

  private[this] def createPost(post: Post):
  (String, PersistentEntityTestDriver[PostCommand, PostEvent, PostState], OutcomeInformation[PostEvent, PostState]) = {
    val entityId = UUID.randomUUID().toString
    val driver = new PersistentEntityTestDriver[PostCommand, PostEvent, PostState](system, new PostEntity(), entityId)
    val outcome = driver.run(CreatePost(post))
    val outcomeInformation = getOutcomeInformation(outcome)
    (entityId, driver, outcomeInformation)
  }

}
