package com.colisweb.post.impl


import java.time.{LocalDate, LocalDateTime}
import java.util.Optional

import akka.japi.Pair
import akka.{Done, NotUsed}
import com.colisweb.ServiceSpec
import com.colisweb.post.api.PostService
import com.colisweb.post.api.PostService.GetPostResponse
import com.colisweb.post.impl.PostServiceImpl.IADVIZE_ISO_LOCAL_DATE_TIME
import com.colisweb.vdm.{VDMService, VDMServiceStub}
import com.google.inject.AbstractModule
import com.lightbend.lagom.javadsl.api.transport.ResponseHeader
import org.scalacheck.Gen
import org.scalatest.prop.PropertyChecks
import play.api.http.Status
import play.inject.guice.GuiceApplicationBuilder

import scala.compat.java8.FutureConverters._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

object PostServiceImplSpec {

  val CONTENT_TYPE_TEXT = "Content-Type"
  val EXPECTED_CONTENT_TYPE = "application/json; charset=utf-8"

  implicit class GetPostResponseWithDate(response: GetPostResponse) {
    def localDate: LocalDate = LocalDateTime.parse(response.date, IADVIZE_ISO_LOCAL_DATE_TIME).toLocalDate
  }

}

class PostServiceImplSpec extends ServiceSpec with PropertyChecks {

  import PostServiceImplSpec._

  class PostServiceImplModule extends AbstractModule {
    def configure() = {
      bind(classOf[VDMService]).to(classOf[VDMServiceStub])
    }
  }

  override def configureBuilder(b: GuiceApplicationBuilder) = b.overrides(new PostServiceImplModule)

  def postService: PostService = server.client(classOf[PostService])

  def scrappingCall(limit: Int): Future[Pair[ResponseHeader, Done]] =
    postService.scrap(limit).withResponseHeader().invoke(NotUsed).toScala

  def searchCall(from: Optional[LocalDate] = Optional.empty(), to: Optional[LocalDate] = Optional.empty(), author: Optional[String] = Optional.empty()) =
    postService.searchPosts(from, to, author).withResponseHeader().invoke(NotUsed).toScala

  it should "scrap posts" in {
    forAll(Gen.chooseNum[Int](0, 10)) { (limit: Int) =>
      val (responseHeader, _) = Await.result(scrappingCall(limit), 10 seconds).toScala
      responseHeader.status() should be(Status.OK)
      responseHeader.getHeader(CONTENT_TYPE_TEXT).get should be(EXPECTED_CONTENT_TYPE)
    }
  }

  it should "scrap and search posts" in {
    Thread.sleep(10000) // We need to wait for propagation in ReadSide.

    val (_, responseBefore) = Await.result(searchCall(), 10 seconds).toScala
    val limit: Int = Gen.chooseNum[Int](2, 10).sample.get
    Await.result(scrappingCall(limit), 10 seconds).toScala

    Thread.sleep(10000)
    // We need to wait for propagation in ReadSide.
    val (responseHeader, responseAfter) = Await.result(searchCall(), 10 seconds).toScala

    responseHeader.status() should be(Status.OK)
    responseHeader.getHeader(CONTENT_TYPE_TEXT).get should be(EXPECTED_CONTENT_TYPE)
    responseAfter.count shouldBe (limit + responseBefore.count)
    responseAfter.posts.distinct.size shouldBe responseAfter.count
  }

  it should "search and get posts" in {
    val (_, response) = Await.result(searchCall(), 10 seconds).toScala

    val individualResponses: Seq[Pair[ResponseHeader, GetPostResponse]] = Await.result(Future.sequence(
      response.posts.map(post => postService.getPost(post.id).withResponseHeader().invoke().toScala)
    ), 10 seconds)

    response.count shouldBe individualResponses.size
    response.posts.toSet shouldBe individualResponses.map(_.second).toSet
  }

  it should "search and filter posts" in {
    val (_, response) = Await.result(searchCall(), 10 seconds).toScala

    val postA = response.posts.head
    val postB = response.posts.last

    Await.result(searchCall(author = Optional.of(postA.author)), 10 seconds).second.posts.contains(postA) shouldBe true
    Await.result(searchCall(author = Optional.of(postA.author)), 10 seconds).second.posts.contains(postB) shouldBe false

    def searchByAuthorAndFrom = Await.result(searchCall(
      from = Optional.of(postA.localDate),
      author = Optional.of(postA.author)
    ), 10 seconds).second.posts

    searchByAuthorAndFrom.contains(postA) shouldBe true

    def searchByAuthorAndTo = Await.result(searchCall(
      to = Optional.of(postA.localDate),
      author = Optional.of(postA.author)
    ), 10 seconds).second.posts

    searchByAuthorAndTo.contains(postA) shouldBe true

    def searchByAuthorAndDate = Await.result(searchCall(
      from = Optional.of(postA.localDate),
      to = Optional.of(postA.localDate),
      author = Optional.of(postA.author)
    ), 10 seconds).second.posts

    searchByAuthorAndDate.contains(postA) shouldBe true

    def searchByDate = Await.result(searchCall(
      from = Optional.of(postA.localDate),
      to = Optional.of(postA.localDate)
    ), 10 seconds).second.posts

    searchByDate.contains(postA) shouldBe true
    searchByDate.contains(postB) shouldBe postB.localDate.isEqual(postA.localDate)

    def searchByFrom = Await.result(searchCall(
      from = Optional.of(postA.localDate)
    ), 10 seconds).second.posts

    searchByFrom.contains(postA) shouldBe true
    searchByFrom.contains(postB) should not be postB.localDate.isBefore(postA.localDate)

    def searchByTo = Await.result(searchCall(
      to = Optional.of(postA.localDate)
    ), 10 seconds).second.posts

    searchByTo.contains(postA) shouldBe true
    searchByTo.contains(postB) should not be postB.localDate.isAfter(postA.localDate)
  }

}
