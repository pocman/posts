package com.colisweb.post.impl

import akka.actor.ActorSystem
import com.lightbend.lagom.javadsl.testkit.PersistentEntityTestDriver._
import org.scalatest.mockito.MockitoSugar
import org.scalatest.{FlatSpec, Matchers}

import scala.collection.JavaConversions

trait EntitySpec extends FlatSpec with Matchers with MockitoSugar {

  val system = ActorSystem.create()

  final case class OutcomeInformation[E, S](
    events: List[E],
    state: S,
    replies: List[Any],
    sideEffects: List[SideEffect]
  )

  def getOutcomeInformation[E, S](outcome: Outcome[E, S]): OutcomeInformation[E, S] = {
    val events = JavaConversions.asScalaBuffer(outcome.events).toList
    val state = outcome.state
    val replies = JavaConversions.asScalaBuffer(outcome.getReplies).toList
    val sideEffects = JavaConversions.asScalaBuffer(outcome.sideEffects).toList
    OutcomeInformation(events, state, replies, sideEffects)
  }

}
