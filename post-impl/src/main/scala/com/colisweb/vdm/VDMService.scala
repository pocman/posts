package com.colisweb.vdm

import akka.NotUsed
import akka.stream.scaladsl.Source
import com.colisweb.post.api.PostService.PostRequest

trait VDMService {

  /**
    * @return Unlimited source of Post from VDM, starting from page 0 to page n...
    */
  def scrapPages: Source[PostRequest, NotUsed]

}
