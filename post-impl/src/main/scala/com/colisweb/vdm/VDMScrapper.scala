package com.colisweb.vdm

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import akka.NotUsed
import akka.stream.scaladsl.Source
import com.colisweb.post.api.PostService.PostRequest
import com.google.inject.Inject
import net.ruippeixotog.scalascraper.browser.JsoupBrowser
import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.model.Element
import net.ruippeixotog.scalascraper.scraper.ContentExtractors.elementList
import play.api.inject.ConfigurationProvider

import scala.collection.immutable.Seq
import scala.concurrent.{ExecutionContext, Future}

class VDMScrapper @Inject()(config: ConfigurationProvider)(implicit ex: ExecutionContext) extends VDMService {

  val vdmFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern(config.get.getString("vdm.datetimeformatter_pattern").get)
  val vdmUrl: String = config.get.getString("vdm.url").get
  val parallelism: Int = config.get.getInt("vdm.parallelism").get

  /**
    * We SHOULD be careful with the parallelisation. We don't want the server to kick us !
    * @return Unlimited source of Post from VDM, starting from page 0 to page n...
    */
  def scrapPages: Source[PostRequest, NotUsed] =
    Source.fromIterator(() => Stream.from(0).toIterator).mapAsync(parallelism)(scrapPage).mapConcat(identity)

  /**
    * The method is not resilient to change in VDM html templating.
    * We SHOULD switch to rss or api sources.
    *
    * @param pageNumber
    * @return
    */
  private def scrapPage(pageNumber: Int): Future[List[PostRequest]] = Future {
    val browser = JsoupBrowser()
    val url = s"$vdmUrl?page=$pageNumber"
    val vdm = browser.get(url)

    val posts: Seq[Element] = vdm >> elementList(".post")

    posts.flatMap { (post: Element) =>
      val content = (post >> element(".content")).text
      (post >> element(".date") >> element("p")).text.split(" / ").toList match {
        case (author: String) :: (date: String) :: tail =>
          List(PostRequest(content, LocalDateTime.parse(date, vdmFormatter), author.replace("Vécue par ", "")))
        case _ => Nil
      }
    }.toList
  }

}
