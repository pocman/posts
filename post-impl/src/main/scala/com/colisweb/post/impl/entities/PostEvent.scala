package com.colisweb.post.impl.entities

import java.util.UUID

import com.lightbend.lagom.javadsl.persistence.{AggregateEvent, AggregateEventTag}
import com.lightbend.lagom.serialization.CompressedJsonable
import org.pcollections.TreePVector

import scala.collection.JavaConverters._

object PostEvent {
  val Tag = AggregateEventTag.of(classOf[PostEvent])
  val AllTags: TreePVector[AggregateEventTag[PostEvent]] = TreePVector.from(List(Tag).asJava)

}

sealed trait PostEvent extends AggregateEvent[PostEvent] with CompressedJsonable {
  override def aggregateTag: AggregateEventTag[PostEvent] = PostEvent.Tag
}

final case class PostCreated(id: UUID, post: Post) extends PostEvent
