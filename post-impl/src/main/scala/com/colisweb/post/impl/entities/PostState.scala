package com.colisweb.post.impl.entities

import java.time.LocalDateTime
import java.util.UUID

case class Post(id: UUID, content: String, date: LocalDateTime, author: String)

case class PostState(mayPost: Option[Post])
