package com.colisweb.post.impl

import com.colisweb.post.api.PostService
import com.colisweb.vdm.{VDMScrapper, VDMService}
import com.google.inject.AbstractModule
import com.lightbend.lagom.javadsl.server.ServiceGuiceSupport

class PostServiceModule extends AbstractModule with ServiceGuiceSupport {

  override def configure(): Unit = {
    bindServices(serviceBinding(classOf[PostService], classOf[PostServiceImpl]))
    bind(classOf[VDMService]).to(classOf[VDMScrapper])
  }

}
