package com.colisweb.post.impl

import java.time.LocalDate
import java.util
import java.util.concurrent.CompletionStage
import java.util.function.Function

import akka.Done
import com.colisweb.post.impl.entities.{PostCreated, PostEvent}
import com.colisweb.post.impl.helpers.CompletionStageConverters
import com.datastax.driver.core.{BoundStatement, PreparedStatement}
import com.datastax.driver.extras.codecs.jdk8.LocalDateCodec
import com.google.common.reflect.TypeToken
import com.google.inject.Inject
import com.lightbend.lagom.javadsl.persistence.cassandra.{CassandraReadSide, CassandraSession}
import com.lightbend.lagom.javadsl.persistence.{AggregateEventTag, ReadSideProcessor}
import org.pcollections.PSequence

import scala.collection.JavaConverters._
import scala.concurrent.ExecutionContext

/**
  * Persistent Entities are used for holding the state of individual entities,
  * but they cannot be used for serving queries that span more than one entity.
  * You need to know the identifier of the entity to be able to interact with it.
  * Therefore you need to create another view of the data that is tailored to the queries that the service provides.
  * Lagom has support for populating this read-side view of the data and also for building queries of the read-side.
  * This separation of the write-side and the read-side of the persistent data is often referred to as the CQRS (Command Query Responibility Segregation) pattern.
  *
  * @param session CassandraSession provides several methods in different flavors for executing queries
  * @param readSide A read-side processor is responsible not just for handling the events produced by the persistent entity,
  *                 it’s also responsible for tracking which events it has handled.
  *                 This is done using offsets.
  * @param ex Future and Promises revolve around ExecutionContexts, responsible for executing computations.
  */
class PostEventProcessor @Inject()(
  session: CassandraSession,
  readSide: CassandraReadSide)(implicit ex: ExecutionContext)
  extends ReadSideProcessor[PostEvent] {

  import CassandraReadSide._
  import CompletionStageConverters._

  @volatile private[this] var writePost: PreparedStatement = _

  private[this] def setWritePost(writePost: PreparedStatement): Unit =
    this.writePost = writePost

  override def aggregateTags(): PSequence[AggregateEventTag[PostEvent]] = PostEvent.AllTags

  val buildHandler: ReadSideProcessor.ReadSideHandler[PostEvent] = {
    readSide
      .builder("post_event_processor_offset")
      .setGlobalPrepare(createTables)
      .setPrepare(prepareWrites)
      .setEventHandler(classOf[PostCreated], processPostCreated)
      .build()
  }

  private[this] def createTables(): CompletionStage[Done] = asCompletionStage {
    session.executeCreateTable(
      "CREATE TABLE IF NOT EXISTS posts (id uuid, content text, date date, author text, PRIMARY KEY (id))"
    ).flatMap(_ =>
      session.executeCreateTable(
        "CREATE INDEX IF NOT EXISTS posts_author_idx ON posts (author);"
      )
    ).flatMap(_ =>
      session.executeCreateTable(
        "CREATE INDEX IF NOT EXISTS posts_dates_idx ON posts (date);"
      )
    )
  }

  private[this] def prepareWrites: Function[AggregateEventTag[PostEvent], CompletionStage[Done]] =
    (tag: AggregateEventTag[PostEvent]) => asCompletionStage {
      for {
        _ <- prepareWritePost(session)
      } yield Done
    }

  private[this] def prepareWritePost(session: CassandraSession) = {
    val statement = session.prepare("INSERT INTO posts (id, content, date, author) VALUES (?, ?, ?, ?)")
    statement.map(ps => {
      setWritePost(ps)
      Done
    })
  }

  def processPostCreated: Function[PostCreated, CompletionStage[util.List[BoundStatement]]] = (evt: PostCreated) => {
    assert(writePost != null)
    writePost.getCodecRegistry.register(LocalDateCodec.instance)
    val localDateToken: TypeToken[LocalDate] = new TypeToken[LocalDate]() {}
    val bindWritePost = writePost.bind()
    bindWritePost.setUUID("id", evt.id)
    bindWritePost.setString("content", evt.post.content)
    bindWritePost.set("date", evt.post.date.toLocalDate, localDateToken)
    bindWritePost.setString("author", evt.post.author)
    completedStatements(List(bindWritePost).asJava)
  }

}
