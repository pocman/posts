package com.colisweb.post.impl.helpers

import java.util.concurrent.CompletionStage

import akka.NotUsed
import akka.japi.Pair
import com.lightbend.lagom.javadsl.api.ServiceCall
import com.lightbend.lagom.javadsl.api.transport.{MessageProtocol, RequestHeader, ResponseHeader}
import com.lightbend.lagom.javadsl.server.HeaderServiceCall
import org.pcollections.HashTreePMap

import scala.compat.java8.FutureConverters.{CompletionStageOps, FutureOps}
import scala.concurrent.{ExecutionContext, Future}
import scala.language.implicitConversions

/**
  * Helps to create ServiceCall out of Future
  */
object ServiceCallConverter extends CompletionStageConverters {

  implicit def liftToServiceCall[Request, Response](f: Request => Future[Response]): ServiceCall[Request, Response] =
    new ServiceCall[Request, Response] {
      def invoke(request: Request): CompletionStage[Response] = f(request)
    }

  implicit def liftToHeaderServiceCall[Request, Response]
  (t: ((RequestHeader, Request) => Future[Response], Int))
    (implicit ec: ExecutionContext): HeaderServiceCall[Request, Response] =
    new HeaderServiceCall[Request, Response] {
      override def invokeWithHeaders(requestHeader: RequestHeader, request: Request): CompletionStage[Pair[ResponseHeader, Response]] = {
        val (f, status) = t
        f(requestHeader, request)
          .map(response => Pair.create(new ResponseHeader(status, new MessageProtocol(), HashTreePMap.empty()), response))
      }
    }
}

/**
  * FutureToCompletionStage convertion (Scala to Java Async)
  */
object CompletionStageConverters extends CompletionStageConverters

trait CompletionStageConverters {

  implicit def asCompletionStage[A](f: Future[A]): CompletionStage[A] = f.toJava

  implicit def asFuture[A](f: CompletionStage[A]): Future[A] = f.toScala

  implicit def asUnusedCompletionStage(f: CompletionStage[_]): CompletionStage[NotUsed] = f.thenApply(_ => NotUsed)
}

