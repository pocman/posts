package com.colisweb.post.impl.entities

import akka.Done
import com.lightbend.lagom.javadsl.persistence.PersistentEntity
import com.lightbend.lagom.serialization.CompressedJsonable

trait PostCommand extends CompressedJsonable

final case class CreatePost(post: Post) extends PersistentEntity.ReplyType[Done] with PostCommand

case class GetPost() extends PersistentEntity.ReplyType[Post] with PostCommand
