package com.colisweb.post.impl

import java.time.LocalDate
import java.time.format.{DateTimeFormatter, DateTimeFormatterBuilder}
import java.util.{Optional, UUID}

import akka.stream.Materializer
import akka.stream.javadsl.Source
import akka.stream.scaladsl.Sink
import akka.{Done, NotUsed}
import com.colisweb.post.api.PostService.{GetPostResponse, PostRequest, SearchPostResponse}
import com.colisweb.post.api._
import com.colisweb.post.impl.entities._
import com.colisweb.post.impl.helpers.ServiceCallConverter
import com.colisweb.vdm.VDMService
import com.google.inject.Inject
import com.lightbend.lagom.javadsl.api.ServiceCall
import com.lightbend.lagom.javadsl.persistence.cassandra.CassandraSession
import com.lightbend.lagom.javadsl.persistence.{PersistentEntityRef, PersistentEntityRegistry, ReadSide}

import scala.collection.immutable.Seq
import scala.compat.java8.FutureConverters._
import scala.compat.java8.OptionConverters._
import scala.concurrent.{ExecutionContext, Future}

object PostServiceImpl {

  def toDomain(post: PostRequest): Post =
    Post(id = UUID.randomUUID(), content = post.content, date = post.date, author = post.author)

  // See : https://groups.google.com/a/lists.datastax.com/forum/#!topic/java-driver-user/pAoLlEzmGWA
  def toDatastax(date: LocalDate): com.datastax.driver.core.LocalDate =
    com.datastax.driver.core.LocalDate.fromDaysSinceEpoch(date.toEpochDay.toInt)


  val IADVIZE_ISO_LOCAL_DATE_TIME: DateTimeFormatter =
    new DateTimeFormatterBuilder()
      .parseCaseInsensitive.append(DateTimeFormatter.ISO_LOCAL_DATE)
      .appendLiteral(' ')
      .append(DateTimeFormatter.ISO_LOCAL_TIME)
      .toFormatter

}

/** PostService implementation
  * Services are implemented by providing an implementation of the service descriptor interface, implementing each call specified by that descriptor.
  *
  * @param persistentEntities A PersistentEntity has a stable entity identifier,
  *                           with which it can be accessed from the service implementation or other places.
  *                           The state of an entity is persistent (durable) using Event Sourcing.
  * @param readSide           A read-side processor is responsible not just for handling the events produced by the persistent entity,
  *                           it’s also responsible for tracking which events it has handled.
  *                           This is done using offsets.
  * @param db                 CassandraSession provides several methods in different flavors for executing queries
  * @param ex                 Future and Promises revolve around ExecutionContexts, responsible for executing computations.
  */
class PostServiceImpl @Inject()(
  persistentEntities: PersistentEntityRegistry,
  readSide: ReadSide,
  db: CassandraSession,
  vdmService: VDMService
)(implicit val ex: ExecutionContext, mat: Materializer) extends PostService {

  import PostServiceImpl._
  import ServiceCallConverter._

  /*
  To access an entity from a service implementation you first need to inject the PersistentEntityRegistry
  and at startup (in the constructor) register the class that implements the PersistentEntity.
   */
  persistentEntities.register(classOf[PostEntity])

  // Once you’ve created your read-side processor, you need to register it with Lagom.
  readSide.register[PostEvent](classOf[PostEventProcessor])

  override def scrap(limit: Int): ServiceCall[NotUsed, Done] = _ => for {
    posts: Seq[Post] <- vdmService.scrapPages.take(limit.toLong).map(toDomain).runWith(Sink.seq[Post])
    responses <- Future.sequence(posts.map(createPost))
  } yield Done

  override def getPost(id: UUID): ServiceCall[NotUsed, GetPostResponse] = _ => getEntityRef(id).map(buildResponse)

  override def searchPosts(from: Optional[LocalDate], to: Optional[LocalDate], author: Optional[String]): ServiceCall[NotUsed, SearchPostResponse] = _ => {
    buildSelectAll(from.asScala, to.asScala, author.asScala)
      .mapAsync(42, getEntityRef(_).toJava)
      .map(buildResponse)
      .runWith(Sink.seq[GetPostResponse], mat)
      .map(posts => SearchPostResponse(posts = posts, count = posts.size))
  }

  /*
  We COULD probably use com.datastax.driver.extras.codecs.jdk8.OptionaCodec to do this job.
  We use LocalDate.MIN and MAX to handle all case (even outside the documented usage).
  In this case, PostgreSQL ReadSide would be better but it's not supported out of the box with `sbt runAll`
  In order to support where clause on multiple none primary keys, we have to use allow filtering.
   */
  private[this] def buildSelectAll(mayFrom: Option[LocalDate], mayTo: Option[LocalDate], mayAuthor: Option[String]): Source[UUID, NotUsed] = {
    ((mayFrom.getOrElse(LocalDate.MIN), mayTo.getOrElse(LocalDate.MAX), mayAuthor) match {
      case (from, to, None) => db.select("SELECT * FROM posts WHERE date >= ? AND date <= ? ALLOW FILTERING", toDatastax(from), toDatastax(to))
      case (from, to, Some(author)) => db.select("SELECT * FROM posts WHERE date >= ? AND date <= ? AND author = ? ALLOW FILTERING", toDatastax(from), toDatastax(to), author)
    }).map(_.getUUID("id"))
  }

  private[this] def buildResponse(post: Post) =
    GetPostResponse(id = post.id, content = post.content, date = post.date.format(IADVIZE_ISO_LOCAL_DATE_TIME), author = post.author)

  private[this] def createPost(post: Post): Future[Done] = postEntityRef(post.id).ask[Done, CreatePost](CreatePost(post))

  private[this] def getEntityRef(id: UUID): Future[Post] = postEntityRef(id).ask[Post, GetPost](GetPost())

  //We use uuid as identifiers, this reduces the risks of id conflicts in a fully async world.
  private[this] def postEntityRef(id: UUID): PersistentEntityRef[PostCommand] = persistentEntities.refFor(classOf[PostEntity], id.toString)

}