package com.colisweb.post.impl.entities


import java.util.{Optional, UUID}

import akka.Done
import com.colisweb.post.api.exceptions.{PostAlreadyCreatedException, PostNotFoundException}
import com.lightbend.lagom.javadsl.persistence.PersistentEntity

import scala.collection.JavaConverters._

/**
  * A simple entity, we COULD use a state machine and change behavior as we go (using a pattern similar to actor behavior transition).
  * See: http://www.lagomframework.com/documentation/1.2.x/java/PersistentEntity.html
  */
class PostEntity extends PersistentEntity[PostCommand, PostEvent, PostState] {
  override def initialBehavior(snapshotState: Optional[PostState]): Behavior = {
    val b = newBehaviorBuilder(snapshotState.orElseGet(() => PostState(Option.empty)))

    b.setCommandHandler(classOf[CreatePost], (cmd: CreatePost, ctx: CommandContext[Done]) => {
      state.mayPost match {
        case Some(_) =>
          ctx.commandFailed(PostAlreadyCreatedException)
          ctx.done()
        case None =>
          val events = PostCreated(id = UUID.fromString(entityId), post = cmd.post) :: Nil
          ctx.thenPersistAll(events.asJava, () => ctx.reply(Done))
      }
    })

    b.setEventHandler(classOf[PostCreated], (evt: PostCreated) => PostState(Some(evt.post)))

    b.setReadOnlyCommandHandler(classOf[GetPost], (cmd: GetPost, ctx: ReadOnlyCommandContext[Post]) =>
      state.mayPost match {
        case None => ctx.commandFailed(PostNotFoundException)
        case Some(p) => ctx.reply(p)
      }
    )

    b.build()
  }
}
