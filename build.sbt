import com.lightbend.lagom.sbt.LagomImport._

organization in ThisBuild := "com.pocman"

// the Scala version that will be used for cross-compiled libraries
scalaVersion in ThisBuild := "2.11.8"
autoCompilerPlugins := true

val jacksonVersion = "2.8.3"

lagomCassandraCleanOnStart in ThisBuild := true

lazy val jacksonParameterNamesJavacSettings = Seq(javacOptions in compile += "-parameters")

def project(id: String, versionV: String) = Project(id = id, base = file(id)).settings(version := versionV)

def scalaProject(id: String, versionV: String) = project(id, versionV)
  .settings(jacksonParameterNamesJavacSettings: _*)
  .settings(
    scalacOptions in Compile += "-Xexperimental", // this enables Scala lambdas to be passed as Java SAMs
    // Come from here:
    //  - https://tpolecat.github.io/2014/04/11/scalac-flags.html
    //  - http://pedrorijo.com/blog/scala-compiler-review-code-warnings/
    scalacOptions ++= Seq(
      "-deprecation",
      "-target:jvm-1.8",
      "-encoding", "UTF-8",
      "-feature",
      "-language:existentials",
      "-language:higherKinds",
      "-language:implicitConversions",
      "-language:postfixOps",
      "-unchecked",
      //      "-Xfatal-warnings",
      "-Xlint",
      "-Xlint:missing-interpolator",
      "-Yno-adapted-args",
      "-Ywarn-unused",
      "-Ywarn-dead-code",
      "-Ywarn-numeric-widen",
      "-Ywarn-value-discard",
      "-Xfuture",
      "-Ywarn-unused-import"
    )
  )
  .settings(
    libraryDependencies ++= Seq(
      "com.fasterxml.jackson.module" %% "jackson-module-scala" % jacksonVersion, // actually, only api projects need this
      "org.scala-lang.modules" % "scala-java8-compat_2.11" % "0.8.0",
      "org.scalacheck" %% "scalacheck" % "1.13.2" % "test",
      "org.scalactic" %% "scalactic" % "3.0.0" % "test",
      "org.scalatest" %% "scalatest" % "3.0.0" % "test",
      "org.mockito" % "mockito-all" % "1.10.19" % "test",
      "com.fortysevendeg" %% "scalacheck-datetime" % "0.2.0" % "test"
    )
  )

def scalaServiceApi(id: String, versionV: String) = scalaProject(id, versionV)
  .settings(libraryDependencies += lagomJavadslApi)

def scalaServiceImpl(id: String, versionV: String) = scalaProject(id, versionV)
  .enablePlugins(LagomJava)
  .settings(
    libraryDependencies ++= Seq(
      lagomJavadslPersistenceCassandra,
      lagomJavadslTestKit
    )
  )
  .settings(lagomForkedTestSettings: _*)

(runAll in ThisBuild) <<= { (runAll in ThisBuild) dependsOn runScrapping }

lazy val runScrapping: TaskKey[Unit] = taskKey[Unit]("launch VDM Scrapping")

runScrapping := {
  val log: Logger = streams.value.log
  Process("./scrap.sh") run log
}

lazy val postApi = scalaServiceApi("post-api", "1.0.0")

lazy val postdImpl = scalaServiceImpl("post-impl", "1.0.0")
  .dependsOn(postApi)
  .settings(
    libraryDependencies ++= Seq(
      "net.ruippeixotog" %% "scala-scraper" % "1.2.0",
      "com.datastax.cassandra" % "cassandra-driver-extras" % "3.1.0"
    )
  )