package com.colisweb.post.api.exceptions

import java.util
import java.util.Optional

import akka.util.ByteString
import com.fasterxml.jackson.databind.ObjectMapper
import com.lightbend.lagom.javadsl.api.deser.RawExceptionMessage
import com.lightbend.lagom.javadsl.api.transport.{MessageProtocol, TransportErrorCode}

class PostServiceExceptionSerializer extends ExternalServiceExceptionSerializer {

  override def canSerialize(exception: Throwable): Boolean = exception.getCause.isInstanceOf[PostException]

  override def serialize(exception: Throwable,
    accept: util.Collection[MessageProtocol],
    objectMapper: ObjectMapper
  ): RawExceptionMessage = {
    val builder = ByteString.newBuilder
    val postException = exception.getCause.asInstanceOf[PostException]
    objectMapper.writeValue(
      builder.asOutputStream,
      postException.toMessage
    )
    new RawExceptionMessage(TransportErrorCode.fromHttp(postException.toMessage.code),
      new MessageProtocol(Optional.of("application/json"), Optional.of("utf-8"), Optional.empty()),
      builder.result())
  }

}

object PostServiceExceptionSerializer {

  def apply(): PostServiceExceptionSerializer = new PostServiceExceptionSerializer()

}
