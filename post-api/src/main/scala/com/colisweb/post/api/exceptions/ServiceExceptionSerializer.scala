package com.colisweb.post.api.exceptions

import java.lang.reflect.InvocationTargetException
import java.util
import java.util.Optional
import java.util.concurrent.{CompletionException, ExecutionException}

import akka.util.ByteString
import com.fasterxml.jackson.annotation.{JsonAutoDetect, PropertyAccessor}
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.lightbend.lagom.javadsl.api.deser.{ExceptionMessage, ExceptionSerializer, RawExceptionMessage}
import com.lightbend.lagom.javadsl.api.transport.{MessageProtocol, TransportErrorCode, TransportException}

import scala.util.{Failure, Success, Try}

final case class ServiceExceptionSerializer(serializers: Seq[ExternalServiceExceptionSerializer]) extends ExceptionSerializer {

  override def serialize(exception: Throwable, accept: util.Collection[MessageProtocol]): RawExceptionMessage = {
    val unwrappedException = unwrap(exception)
    val maybeSerializer = serializers.find(_.canSerialize(unwrappedException))
    maybeSerializer match {
      case Some(serializer) => serializer.serialize(unwrappedException, accept, ServiceExceptionSerializer.objectMapper)
      case None => ServiceExceptionSerializer.defaultSerializer(unwrappedException, accept)
    }
  }

  override def deserialize(message: RawExceptionMessage): Throwable = {
    val maybeExceptionMessage = Try(
      ServiceExceptionSerializer.objectMapper.readValue(
        message.message().iterator.asInputStream,
        classOf[ExceptionMessage]
      )
    )
    maybeExceptionMessage match {
      case Failure(_) =>
        val genericExceptionMessage = new ExceptionMessage("UndeserializableException", message.message().utf8String)
        TransportException.fromCodeAndMessage(message.errorCode(), genericExceptionMessage)
      case Success(realExceptionMessage) => TransportException.fromCodeAndMessage(message.errorCode(), realExceptionMessage)
    }
  }

  private[this] def unwrap(exception: Throwable): Throwable = {
    if (exception.getCause != null) {
      exception match {
        case _: ExecutionException | _: InvocationTargetException | _: CompletionException => unwrap(exception.getCause)
        case _ => ()
      }
    }
    exception
  }

}

object ServiceExceptionSerializer {

  val objectMapper = new ObjectMapper()
    .setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY)
    .registerModule(new Jdk8Module())

  // TODO a certain amount of code can be abstracted here
  def defaultSerializer(exception: Throwable, accept: util.Collection[MessageProtocol]): RawExceptionMessage = {
    val builder = ByteString.newBuilder
    exception match {
      case e: TransportException => objectMapper.writeValue(builder.asOutputStream, e.exceptionMessage())
      case e => objectMapper.writeValue(builder.asOutputStream, UnknownInternalError)
    }
    new RawExceptionMessage(TransportErrorCode.fromHttp(500),
      new MessageProtocol(Optional.of("application/json"), Optional.of("utf-8"), Optional.empty()),
      builder.result())
  }

}
