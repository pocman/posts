package com.colisweb.post.api

import java.time.{LocalDate, LocalDateTime}
import java.time.format.DateTimeFormatter
import java.util.{Optional, UUID}

import akka.{Done, NotUsed}
import com.colisweb.post.api.exceptions.{PostServiceExceptionSerializer, ServiceExceptionSerializer}
import com.lightbend.lagom.javadsl.api.ScalaService._
import com.lightbend.lagom.javadsl.api.deser.PathParamSerializers
import com.lightbend.lagom.javadsl.api.transport.Method
import com.lightbend.lagom.javadsl.api.{Descriptor, Service, ServiceCall}

object PostService {

  final case class PostRequest(content: String, date: LocalDateTime, author: String)

  // We COULD use custom LocalDateTime Serializer for date field, but jackson annotation are annoying.
  final case class GetPostResponse(id: UUID, content: String, date: String, author: String)

  final case class SearchPostResponse(posts: Seq[GetPostResponse], count: Int)

  private val LocalDateSerializer =
    PathParamSerializers.required[LocalDate]("LocalDate", (date: String) => LocalDate.parse(date), (date: LocalDate) => date.format(DateTimeFormatter.ISO_DATE))

  private val UUIDSerializer =
    PathParamSerializers.required[UUID]("UUID", (id: String) => UUID.fromString(id), (id: UUID) => id.toString)


}

trait PostService extends Service {

  import PostService._

  /**
    * Scraps the limit'th first VDM posts and creates an entity for each of them.
    * @param limit number of post to scrap
    * @return A future when process is finish and commands are accepted by service
    */
  def scrap(limit: Int): ServiceCall[NotUsed, Done]

  /**
    * getPost is fully consistent with the scrapping even if service are distributed across nodes,
    * enforcing consistency requires to query the Post by its entity UUID.
    * Incremental Integer Id are not suitable for distributed systems but we COULD use a shared singleton across the akka cluster to solve this issue.
    *
    * @param id the sequential unique identifier of the post
    * @return the requested post or a 404 error
    */
  def getPost(id: UUID): ServiceCall[NotUsed, GetPostResponse]

  /**
    * searchPosts is NOT fully consistent with the scrapping. It takes time to propaged events to the ReadSide.
    *
    * @param from filters posts published strictly before this date
    * @param to filters posts published strictly after this date
    * @param author filters posts by author (case sensitive)
    * @return
    */
  def searchPosts(from: Optional[LocalDate], to: Optional[LocalDate], author: Optional[String]): ServiceCall[NotUsed, SearchPostResponse]

  /**
    * The descriptor associated with the requested API.
    */
  override def descriptor(): Descriptor =
    named("Post").withCalls(
      restCall(Method.GET, "/api/scrap?limit", scrap _),
      restCall(Method.GET, "/api/posts/:id", getPost _),
      restCall(Method.GET, "/api/posts?from&to&author", searchPosts _)
    )
      .withAutoAcl(true)
      .withPathParamSerializer[LocalDate](classOf[LocalDate], LocalDateSerializer)
      .withPathParamSerializer(classOf[UUID], UUIDSerializer)
      .withExceptionSerializer(ServiceExceptionSerializer(Seq(PostServiceExceptionSerializer())))

}
