package com.colisweb.post.api.exceptions

import java.util

import com.fasterxml.jackson.databind.ObjectMapper
import com.lightbend.lagom.javadsl.api.deser.RawExceptionMessage
import com.lightbend.lagom.javadsl.api.transport.MessageProtocol

trait ExternalServiceExceptionSerializer {

  def canSerialize(exception: Throwable): Boolean

  def serialize(exception: Throwable,
                accept: util.Collection[MessageProtocol],
                objectMapper: ObjectMapper): RawExceptionMessage

}
