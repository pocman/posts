package com.colisweb.post.api.exceptions


case class PostExceptionMessage(code: Int, message: String, documentation: String) extends Serializable

trait PostException extends RuntimeException {
  def toMessage: PostExceptionMessage
}

object UnknownInternalError extends PostException {
  def toMessage = PostExceptionMessage(500, "Unknown internal error", "")
}

object PostNotFoundException extends PostException {
  def toMessage = PostExceptionMessage(404, "Post not found", "")
}

object PostAlreadyCreatedException extends PostException {
  def toMessage = PostExceptionMessage(409, "Post already exists", "")
}
